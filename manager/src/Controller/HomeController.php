<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Homepage
 */
class HomeController extends AbstractController
{


    /**
     * @Route("/", name="homepage")
     */
    public function index(): Response
    {
        print_r('works');
        return new Response('', Response::HTTP_OK);
    }
}
